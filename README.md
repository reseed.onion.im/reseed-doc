#About my Reseed

## Hosting
Hosting is via DigitalOcean.
The bootstrap scripts are publicly [available][bootstrap].

## Monitoring
Once a day an [AWS Lambda][lambda] is configured to download a SU3 on my server and check that it's fresh.

### Logging
There is no user data to log.
Regardless, the log group is set to remove logs after 1 week.

[bootstrap]: https://gitlab.com/reseed.onion.im/reseed-bootstrap
[lambda]: https://gitlab.com/reseed.onion.im/reseed-lambda

## License
[Creative Commons Zero v1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)
